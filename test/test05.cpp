// vim: set cin et ts=4 sw=4 tw=80:

#include <iostream>
#include <iomanip>
#include <armadillo>
#include "../src/hankeltrans.hpp"

using namespace std;
using namespace o2scl;
using namespace arma;

int main(int argc, const char * argv[])
{
    size_t NS = 101;
    vec b(NS);
    for (size_t i=0; i < NS; ++i)
    {
        b[i] = (0.0 + i*20.0/(NS-1));
    }

    // HTr(r^3) = 9/k^5
    funct f1 = [] (const double &x) { return exp(-x*x*20.0); };
    HankelTrOg_K<> H1 (f1, 3E-3);
    H1.setTolAbs(1E-6);
    H1.setTolRel(0);
    H1(b);
    vec analres = 0.025 * exp(-pow(b,2) * 0.0125);
    for (size_t i=0; i < b.size(); ++i)
    {
        double t = analres[i];
        cout.setf(ios_base::fixed, ios_base::floatfield);
        cout.precision(1);
        cout << setw(4) << b.at(i);
        cout.setf(ios_base::scientific, ios_base::floatfield);
        cout.precision(5);
        cout << setw(15)
             << t                          << setw(15)
             << H1.getRes().at(i)          << setw(15)
             << abs(H1.getRes().at(i) - t) / t;
        cout.unsetf(ios_base::scientific);
        cout << setw(15) << H1.getNodesRec()[i] << endl;
    }

    auto cmp = arma::approx_equal(analres, H1.getRes(), "both",
                                  H1.getTolAbs()*10,
                                  H1.getTolRel()*10);
    cout << (cmp ? "Test OK" : "Error" ) << endl;

    return 0;
}
