// vim: set cin et ts=4 sw=4 tw=80:

#include <iostream>
#include <iomanip>
#include <armadillo>
#include "../src/hankeltrans.hpp"

using namespace std;
using namespace o2scl;
using arma::vec;

int main(int argc, const char * argv[])
{
    size_t NS = 100;
    vec k(NS);
    for (size_t i=0; i < NS; ++i)
    {
        k[i] = (1.0 + i*9.0/(NS-1));
    }

    // HTr(r) = -1/k^3
    funct f1 = [] (const double &x) { return x; };
    HankelTrOg_K<> H1 (f1, 0.02);
    H1.setTolAbs(1E-9);
    H1.setTolRel(0);
    H1(k);
    cout.setf(ios_base::scientific);
    cout.precision(5);
    vec analres = -1.0/pow(k,3);
    for (size_t i=0; i < k.size(); ++i)
    {
        double t = analres[i];
        cout.setf(ios_base::scientific);
        cout.precision(5);
        cout << t                          << setw(15)
             << H1.getRes().at(i)          << setw(15)
             << abs(H1.getRes().at(i) - t);
        cout.unsetf(ios_base::scientific);
        cout << setw(15) << H1.getNodesRec()[i] << endl;
    }

    auto cmp = arma::approx_equal(analres, H1.getRes(), "absdiff",
                                  H1.getTolAbs());
    cout << (cmp ? "Test OK" : "Error" ) << endl;



    return 0;
}
