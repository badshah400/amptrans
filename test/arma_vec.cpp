// vim: set et cin ts=4 sw=4 tw=80:

#include <armadillo>

using namespace arma;

int main(int argc, char const * argv[])
{
    vec A = {1, 2, 3, 4};
    A.print("## A ##");
    vec B = 1+A;
    B.print("## 1+A ##");
    B = A % (-A);
    B.print("## A .* (-A) ##");
    B = 2 * cos(A) % sin(A);
    B.print("## 2*cos(A)*sin(A) ##");
    auto C = approx_equal (B, sin(2*A), "absdiff", 1E-10);
    cout << "## 2*sin(A)*cos(A) == sin(2*A)? ##" << endl;
    cout << C << endl;

	return 0;
}
