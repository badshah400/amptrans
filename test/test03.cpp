// vim: set cin et ts=4 sw=4 tw=80:

#include <iostream>
#include <iomanip>
#include <armadillo>
#include "../src/hankeltrans.hpp"

using namespace std;
using namespace o2scl;
using namespace arma;

int main(int argc, const char * argv[])
{
    size_t NS = 1;
    vec k = ones<vec>(NS);

    // HTr(1/(r^2+1)) at k = 1: K0(1) is the modified Bessel function of order 0
    // of the second kind
    // Not expected to converge rapidly: See Ogata 2005, Sec. 5
    funct f1 = [] (const double &x) { return 1.0 / (x*x + 1.0); };
    HankelTrOg_K<> H1 (f1, 2.0E-2, 1000);
    H1.setTolAbs(1E-6);
    H1.setTolRel(0);
    H1(k);
    double t = boost::math::cyl_bessel_k(0.0, k(0));
    for (size_t i=0; i < k.size(); ++i)
    {
        cout.setf(ios_base::scientific);
        cout.precision(5);
        cout << t                          << setw(15)
             << H1.getRes().at(i)          << setw(15)
             << abs(H1.getRes().at(i) - t) << endl;
    }
    cout << "Abs Tolerance: " << H1.getTolAbs() << endl;

    cout.unsetf(ios_base::scientific);
    cout << "Number of nodes used: " << H1.getNodesRec().back() << endl;

    auto cmp = arma::approx_equal(vec{ t }, H1.getRes(), "absdiff",
                                  H1.getTolAbs());
    cout << (cmp ? "Test OK" : "Error" ) << endl;

    return 0;
}
