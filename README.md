# Hankel Transforms of Scattering Amplitudes in QCD #

We implement Hankel transforms of hard pomeron scattering in QCD
by converting the slowly convergent integral into a comparatively
faster convergent series using the method of Ogata 2005.

## License ##

This code is distributed under the terms of the `MIT License.`

Please see the `License.txt` file for terms and conditions.

## Dependencies ##

* Boost
* GSL
* O2Scl
* Armadillo

## References ##

Based on the algorithm provided in

> **H. Ogata**, A Numerical Integration Formula Based on the Bessel
> Functions, Publications of the Research Institute for Mathematical
> Sciences, vol. 41, no. 4, pp. 949-970, 2005.

