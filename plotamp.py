#!/usr/bin/env python3
# vim: set ai et ts=4 sw=4 tw=80:

import numpy as np
import sys
from matplotlib import pyplot as plt

plt.rc('text', usetex=True)

finame = sys.argv[1] if len(sys.argv) > 1 else "out.dat"

dat = np.loadtxt(finame)
plt.xlabel("$x$")
plt.ylabel("$\log(y)$")
plt.semilogy(dat[:,0], dat[:,1], ls='-', lw=1, color='olive')
outf = finame.rsplit('.')[0] + '.pdf'
plt.savefig(outf)
