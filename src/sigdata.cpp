// vim: set cin et ts=4 sw=4 tw=80:

#include "sigdata.hpp"

using namespace std;

void sigdata::set_el(const vector<string> & fnames)
{
    vvdouble data;
    vec data_sqrt_s, data_sig_mb, err_max, err_min, delta;

    for (auto const f : fnames)
    {
	    vvdouble f_data = tabdatr(f, 9);
        vec f_sqrt_s    = icoldata(f_data, 1),
            f_sig_mb    = icoldata(f_data, 4),
            f_err_max   = sqrt(square(vec(icoldata(f_data,5)))
                          + square(vec(icoldata(f_data, 7)))),
            f_err_min   = sqrt(square(vec(icoldata(f_data,6)))
                          + square(vec(icoldata(f_data, 8)))),
            f_delta     = f_err_max + f_err_min;

        f_sqrt_s.transform([] (double val) { return sqrt(pLAB2s(val)); });
        arma::uvec thr_idx = arma::find(f_sqrt_s > this->sqrt_s_min);

        data_sqrt_s.insert_rows(data_sqrt_s.n_elem, f_sqrt_s.elem(thr_idx));
        data_sig_mb.insert_rows(data_sig_mb.n_elem, f_sig_mb.elem(thr_idx));
        delta.insert_rows(delta.n_elem, f_delta.elem(thr_idx));
    }
    this->data_el.clear();

    this->data_el = arma::join_rows(data_sqrt_s, data_sig_mb);
    this->data_el = arma::join_rows(this->data_el, delta);
}

void sigdata::set_tot(const vector<string> & fnames)
{
    vvdouble data;
    vec data_sqrt_s, data_sig_mb, err_max, err_min, delta;

    for (auto const f : fnames)
    {
	    vvdouble f_data = tabdatr(f, 9);
        vec f_sqrt_s    = icoldata(f_data, 1),
            f_sig_mb    = icoldata(f_data, 4),
            f_err_max   = sqrt(square(vec(icoldata(f_data,5)))
                          + square(vec(icoldata(f_data, 7)))),
            f_err_min   = sqrt(square(vec(icoldata(f_data,6)))
                          + square(vec(icoldata(f_data, 8)))),
            f_delta     = f_err_max + f_err_min;

        f_sqrt_s.transform([] (double val) { return sqrt(pLAB2s(val)); });
        arma::uvec thr_idx = arma::find(f_sqrt_s > this->sqrt_s_min);

        data_sqrt_s.insert_rows(data_sqrt_s.n_elem, f_sqrt_s.elem(thr_idx));
        data_sig_mb.insert_rows(data_sig_mb.n_elem, f_sig_mb.elem(thr_idx));
        delta.insert_rows(delta.n_elem, f_delta.elem(thr_idx));
    }
    this->data_tot.clear();

    this->data_tot = arma::join_rows(data_sqrt_s, data_sig_mb);
    this->data_tot = arma::join_rows(this->data_tot, delta);
}

double pLAB2s(const double & pL, const double & m1, const double & m2)
{
    double EL = sqrt(sqr(pL) + sqr(m1)) + m2;
    double s = (sqr(m1) + sqr(m2) + 2*EL*m2);
    return s;
}

