// vim: set cin et ts=4 sw=4 tw=80:

#pragma once

#include <functional>
#include <o2scl/inte_qagiu_gsl.h>
#include <o2scl/inte_qag_gsl.h>
#include <o2scl/inte_qags_gsl.h>
#include <o2scl/inte_adapt_cern.h>
#include <o2scl/inte_gauss56_cern.h>
#include <boost/math/special_functions/bessel.hpp>
#include <boost/math/constants/constants.hpp>
#include <armadillo>
#include <utility>
#include "debug.h"

#ifndef vecd
typedef std::vector<double> vecd;
typedef arma::vec::iterator vec_it;
#endif

using namespace o2scl;
using namespace boost::math::policies;

using arma::vec;
using boost::math::double_constants::pi;
using boost::math::double_constants::half_pi;

typedef policy<promote_double<false>, digits10<12> > fast_pol;

template <int nu = 0>
class HankelTr_K: public inte_qagiu_gsl<>, public inte_gauss56_cern<>
{
    public:
        HankelTr_K(const size_t n = HankelTr_K<nu>::MaxNodes) : Nodes(n)
        {  }
        HankelTr_K(funct & ff, const size_t n = HankelTr_K<nu>::MaxNodes) : fint(ff), Nodes(n)
        {  }

        virtual void operator() (const vec & k);

        inline vec getRes() const { return this->res; }
        inline vec getErr() const { return this->err; }
        void setFunc(funct & f)
        { fint = f; }
        inline void setTolAbs(const double & tol)
        {
            inte_gauss56_cern<>::tol_abs = inte_qagiu_gsl<>::tol_abs = tol;
        }
        inline void setTolRel(const double & tol)
        {
            inte_gauss56_cern<>::tol_rel = inte_qagiu_gsl<>::tol_rel = tol;
        }
        inline double getJ0Roots(size_t i) const
        {
            return boost::math::cyl_bessel_j_zero(double(nu), i+1, fast_pol());
        }
        inline vec getJ0Roots() const
        {
            return BessJ0Roots;
        }
        inline vec getJ0Extrm() const
        {
            return BessJ0Extrm;
        }
        inline std::vector<size_t> getNodesRec() const { return NodesRec; }
        inline double getTolAbs() const
        {
            return std::max(inte_gauss56_cern<>::tol_abs,
                            inte_qagiu_gsl<>::tol_abs);
        }
        inline double getTolRel() const
        {
            return std::max(inte_gauss56_cern<>::tol_rel,
                            inte_qagiu_gsl<>::tol_rel);
        }
        virtual ~HankelTr_K()
        {
            res.clear();
            err.clear();
            BessJ0Roots.clear();
            BessJ0Extrm.clear();
        }

    protected:
        funct fint;
        vec res;
        vec err;
        vec BessJ0Roots;
        vec BessJ0Extrm;
        std::vector<size_t> NodesRec;

        void setReqNodes(o2scl::funct &);
        void setReqNodes(const size_t &);

        std::pair<double,double> tr_at_0k();

        std::pair<double,double> small_k_tr(const double & k,
                                            size_t & MaxIters = 50);

        static constexpr size_t MaxNodes = 5E2;
    private:
        size_t Nodes;
};

template <int nu>
std::pair<double,double> HankelTr_K<nu>::tr_at_0k()
{
    double resi, erri;
    funct f = [this] (const double & x)
    {
        return x * this->fint(x);
    };
    inte_qagiu_gsl<>::integ_err(f, 0.0, 0.0, resi, erri);
    return std::make_pair(resi
                          * boost::math::cyl_bessel_j(nu, 0.0, fast_pol()),
                          erri);
}

template <int nu>
void HankelTr_K<nu>::setReqNodes(o2scl::funct & f)
{
    using namespace boost::math;
    using namespace std;
    size_t ReqNodes = 0;
    double h0, h1, f0, f1;
    do
    {
        ReqNodes++;
        h1 = cyl_bessel_j_zero(double(nu), ReqNodes+1, fast_pol())
             - cyl_bessel_j_zero(double(nu), ReqNodes, fast_pol());
        if (ReqNodes==1)
        {
            h0 = 0.5 * cyl_bessel_j_zero(double(nu), ReqNodes, fast_pol());
        }
        else
        {
            h0 = cyl_bessel_j_zero(double(nu), ReqNodes, fast_pol())
                 - cyl_bessel_j_zero(double(nu), ReqNodes-1, fast_pol());
        }
        f1 = f(cyl_bessel_j_zero(double(nu+1.0), ReqNodes+1, fast_pol()));
        f0 = f(cyl_bessel_j_zero(double(nu+1.0), ReqNodes, fast_pol()));
        if (__DEBUG__)
        {
            cout.setf(ios_base::scientific);
            cout << abs(f1*h1/f0/h0) << endl;
        }

    } while(abs(f1*h1) / abs(f0*h0)
            > inte_qagiu_gsl<>::tol_abs);
    BessJ0Roots.resize(std::min(ReqNodes,Nodes));
    BessJ0Extrm.resize(std::min(ReqNodes,Nodes));
    cyl_bessel_j_zero(double(nu+1), 1, BessJ0Extrm.size(), BessJ0Extrm.begin());
    cyl_bessel_j_zero(double(nu), 1, BessJ0Roots.size(), BessJ0Roots.begin());
    NodesRec.push_back(ReqNodes);
}

template <int nu>
void HankelTr_K<nu>::setReqNodes(const size_t & maxnodes)
{
    using boost::math::cyl_bessel_j_zero;
    BessJ0Roots.resize(maxnodes);
    cyl_bessel_j_zero(double(nu), 1, BessJ0Roots.size(), BessJ0Roots.begin());
}

template <int nu>
void HankelTr_K<nu>::operator() (const vec & kk)
{
    using namespace std;
    NodesRec.clear();
    res.resize(kk.size());
    err.resize(kk.size());
    vec_it it_res = res.begin(),
           it_err = err.begin();

    for (double k : kk)
    {
        if (abs(k) < numeric_limits<double>::epsilon())
        {
            auto zerok = tr_at_0k();
            *it_res = zerok.first;
            *it_err = zerok.second;
            it_res++;
            it_err++;
            NodesRec.push_back(0);
        }
        else
        {
            funct f = [this, k] (const double & x)
            {
                using boost::math::cyl_bessel_j;
                double r = x * this->fint(x/k);
                if (abs(r) < inte_qagiu_gsl<>::tol_abs)
                    return 0.0;
                double b = cyl_bessel_j(0, x, fast_pol());
                r *= b;
                return r;
            };

            setReqNodes(f);

            // SEGMENTED SUMMATION OF INTEGRATION BETWEEN NODES
            // [DOESN'T WORK AT LOW k < 0.1]

            double resi, erri;
            inte_qag_gsl<> ig0;
            ig0.tol_abs = inte_qagiu_gsl<>::tol_abs;
            ig0.tol_rel = inte_qagiu_gsl<>::tol_rel;
            ig0.integ_err(f, 0, BessJ0Roots[0], resi, erri);

            for (size_t i = 1; i < getJ0Roots().size()-1; i+=2)
            {
                double tmp;
                inte_gauss56_cern<>::integ_err(f, BessJ0Roots[i-1],
                                               BessJ0Roots[i+1], tmp, erri);
                resi += tmp;
            }

            *it_res = resi / k / k;
            *it_err = erri;
            it_res++;
            it_err++;
        }
    }
}

template <int nu = 0>
class HankelTrOg_K: public HankelTr_K<nu>
{
    public:
        HankelTrOg_K(const double & h=HankelTrOg_K::DefH,
                     const size_t & nmax=HankelTrOg_K<nu>::MaxNodes)
            : HankelTr_K<nu>(nmax), H(h), N(nmax)
        {
            this->setReqNodes(N);
        }
        HankelTrOg_K(funct & ff,
                     const double & h=HankelTrOg_K<nu>::DefH,
                     const size_t & nmax=HankelTrOg_K<nu>::MaxNodes)
            : HankelTr_K<nu>(ff, nmax), H(h), N(nmax)
        {
            this->setReqNodes(N);
        }
        void operator() (const vec & k) override;
        inline void setNumNodes(const size_t N) { return; }
        ~HankelTrOg_K() { }

    protected:
        std::vector<double> BessJ0Roots;
        std::vector<double> BessJ0Weights;

        inline double psi(const double & x)
        {
            return x * tanh(half_pi * sinh(x));
        }

        inline vec psi(const vec & x)
        {
            return x % tanh(half_pi * sinh(x));
        }

        double dpsi(const double & x)
        {
            double a = (pi*(x*cosh(x)) + sinh(pi * sinh(x)))
                        / (1.0 + cosh(pi*sinh(x)));
            if (std::isnan(a))
                a = 1.0;
            return a;
        }

        vec dpsi(const vec & x)
        {
            vec a = (pi*(x%cosh(x)) + sinh(pi * sinh(x)))
                    / (1.0 + cosh(pi*sinh(x)));
            a.replace(arma::datum::nan, 1.0);
            return a;
        }

        double weight(const double & x)
        {
            return (boost::math::cyl_neumann(nu, pi * x, fast_pol())
                    / boost::math::cyl_bessel_j(nu+1, pi * x, fast_pol()));
        }

        vec weight(size_t & n)
        {
            using namespace boost::math;
            vec v(HankelTr_K<nu>::getJ0Roots());
            v.resize(n);
            return (this->cyl_neumann(nu, pi * v)
                    / this->cyl_bessel_j(nu+1, pi * v));
        }

    private:
        double H;
        const size_t N;
        static constexpr double DefH = 5.0E-2;
        vec cyl_bessel_j(const vec & x)
        {
            vec res(x);
            res.transform( [] (double & val)
            {
                 return boost::math::cyl_bessel_j(nu, val, fast_pol());
            } );
            return res;
        }

        vec cyl_neumann(const vec & x)
        {
            vec res(x);
            res.transform( [] (double & val)
            {
                 return boost::math::cyl_neumann(nu, val, fast_pol());
            } );
            return res;
        }

};

template <int nu>
void HankelTrOg_K<nu>::operator() (const vec & kk)
{
    using namespace std;
    HankelTr_K<nu>::res.zeros(kk.size());
    HankelTr_K<nu>::err.zeros(kk.size());
    HankelTr_K<nu>::NodesRec.resize(kk.size());
    vec_it it_res   = HankelTr_K<nu>::res.begin(),
           it_err   = HankelTr_K<nu>::err.begin();
    vector<size_t>::iterator it_nodes = HankelTr_K<nu>::NodesRec.begin();

    for (double k : kk)
    {
        if (abs(k) < numeric_limits<double>::epsilon())
        {
            auto reserr = HankelTr_K<nu>::tr_at_0k();
            *it_res = reserr.first;
            *it_err = reserr.second;
            it_res++;
            it_err++;
            *it_nodes = 0;
            it_nodes++;
            continue;
        }
        if (k < 1)
        {
            // Use plain old segmented sum for low k
            size_t iters;
            auto v  = HankelTr_K<nu>::small_k_tr(k, iters);
            *it_res = v.first;
            *it_err = v.second;
            it_res++;
            it_err++;
            *it_nodes = iters;
            it_nodes++;
            continue;
        }
        //else
        {
            size_t iNode = 1;

            do
            {
                double Hi = H;
                double j0root = 0,
                       w0     = 0;
                if (iNode > BessJ0Roots.size())
                {
                    j0root = boost::math::cyl_bessel_j_zero(double(nu), 
                                                            iNode, fast_pol())
                             / pi;
                    BessJ0Roots.push_back(j0root);
                    w0     = weight(j0root);
                    BessJ0Weights.push_back(w0);
                }
                else
                {
                    j0root = BessJ0Roots[iNode-1];
                    w0     = BessJ0Weights[iNode-1];
                }
                double xeval   = pi * psi( Hi * j0root ) / Hi,
                       fsc     = xeval * HankelTr_K<nu>::fint(xeval/k),
                       j0val   = boost::math::cyl_bessel_j(nu, xeval, fast_pol()),
                       psipr   = dpsi(Hi*j0root);

                double resi    = pi * w0 * fsc * j0val * psipr / (k * k);
                *it_res       += resi;
                double abserri = abs(resi),
                       relerri = abserri/abs(*it_res);

                if (__DEBUG__)
                {
                    cout << iNode;
                    cout.setf(ios_base::scientific);
                    cout.precision(5);
                    cout << setw(20) << abserri << endl;
                }

                if ( (abserri < this->getTolAbs())
                        || (relerri < this->getTolRel()) || (iNode >= N) )
                {
                    *it_err = abserri;
                    it_res++;
                    it_err++;
                    break;
                }
                iNode++;
            } while ( true );

           *it_nodes = iNode;
           it_nodes++;

        }
    }
}

template<int nu>
std::pair<double, double> HankelTr_K<nu>::small_k_tr(const double & k,
                                                     size_t & MaxIters)
{
    using boost::math::cyl_bessel_j_zero;
    funct f = [this, k] (const double & x)
    {
        using boost::math::cyl_bessel_j;
        double r = x * this->fint(x);
        if (abs(r) < inte_qagiu_gsl<>::tol_abs)
            return 0.0;
        double b = cyl_bessel_j(0, x*k, fast_pol());
        r *= b;
        return r;
    };

    double resi(0.0), erri(1.0);
    inte_qag_gsl<> ig0;
    ig0.tol_abs = inte_qagiu_gsl<>::tol_abs;
    ig0.tol_rel = inte_qagiu_gsl<>::tol_rel;
    ig0.integ_err(f, 0, cyl_bessel_j_zero(0.0, 1) / k, resi, erri);

    // Set erri to huge val
    erri = std::numeric_limits<double>::max();

    size_t i = 1;
    for (i = 1; (i < BessJ0Roots.size()) && (erri > std::max(ig0.tol_abs,ig0.tol_rel));
         i+=2)
    {
        double tmp;
        inte_gauss56_cern<>::integ_err(f, BessJ0Roots.at(i)/k,
                                       BessJ0Roots.at(i+2)/k, tmp, erri);
        resi += tmp;
        erri  = abs(tmp / resi);
        // printf("%.3f%15.3E%15.3E%15d\n", k, tmp, erri, i);
    }
    MaxIters = i;

    return std::make_pair(resi, erri);
}
