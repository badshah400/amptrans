// vim: set cin et ts=4 sw=4 tw=80:

#pragma once

#include <cmath>
#include <functional>
#include <utility>
#include "misc.hpp"
#include "hankeltrans.hpp"
#include "debug.h"

enum class UTScheme {EIKONAL = 0, UMATRIX};
enum XSec {EL = 0, TOT};

// See https://arxiv.org/pdf/hep-ph/0612046.pdf for form, params
// amp_func_def represents the amplitude $A(s,q)$ in
// $ (1/4\pi) \int d^2 q A(s,q) J_{0}(b q) $.
// The final result after transform is to be divided by 4\pi in func
// hankeltr().
std::function default_amp_func
    = [] (const double & s, const double & q)
      {
          double t = -1.0 * q * q; // Mandelstam variable t

          // $ A(s,t) = s^{1 + \epsilon + 0.5 b_{1} t} / s $
          // See, e.g., Donnachie and Landshoff, Phys.Lett. B727 (2013) 500-505
          double ep = 0.1, b_1 = 0.55, t_0 = 0.7;
          double res = pow(s,
                           ep + 0.5*b_1*t)
                       / pow(1 - t/t_0, 4);

          // Really small numbers slow up integration, trim them
          // off
          if (abs(res) < std::numeric_limits<double>::epsilon())
            return 0.0;

          return res;
      };


class AmpK : public HankelTrOg_K<>
{
    public:
        AmpK (const double & sqrt_s,
              const double & DelH = 1E-3,
              const arma::vec & b = arma::linspace(0, 20, 201))
            : HankelTrOg_K(DelH),
              S(sqrt_s * sqrt_s),
              bvec(b),
              errvec(b.n_elem)
        {
            this->setFunc(default_amp_func);
            HankelTrOg_K<>::setFunc(this->AmpFunc);
        }

        void setFunc(std::function<double(const double &, const double &)> func)
        {
            using namespace std::placeholders;
            this->AmpFunc = std::bind(func, this->S, _1);
            HankelTrOg_K<>::setFunc(this->AmpFunc);
        }

        double operator () (const double & q) const
        {
            return HankelTrOg_K<>::fint(q);
        }

        vec hankeltr(const vec & k)
        {
            HankelTrOg_K<>::operator()(k);
            return HankelTrOg_K<>::getRes() / (4 * M_PI);
        }

        vec hankeltr(const vec & k, vec & err)
        {
            HankelTrOg_K<>::operator()(k);
            err = HankelTrOg_K<>::getErr();
            return HankelTrOg_K<>::getRes() / (4 * M_PI);
        }

        std::pair<double,double> sig(const UTScheme & S)
        {
            this->setGsb(S);
            // RUN BASIC TRAPEZOIDAL TO DO THE INTEGRAL TO GET SIG EL AND TOT
            //
            double del_b = this->bvec(1) - this->bvec(0);
            auto sig_el  = o2scl::vector_integ_extended8(this->bvec.n_elem,
                                                         (this->Gsb
                                                          % this->Gsb
                                                          % this->bvec))
                           * 2 * M_PI * InvGeV2_to_mb * del_b;
            auto sig_tot = o2scl::vector_integ_extended8(this->bvec.n_elem,
                                                         this->Gsb % this->bvec)
                           * 4 * M_PI * InvGeV2_to_mb * del_b;
            return std::make_pair(sig_el,sig_tot);
        }

        double sig_el(const UTScheme & S = UTScheme::EIKONAL)
        {
            this->setGsb(S);
            // RUN BASIC TRAPEZOIDAL TO DO THE INTEGRAL TO GET SIG EL AND TOT
            //
            double del_b = this->bvec(1) - this->bvec(0);
            auto sig_el  = o2scl::vector_integ_extended8(this->bvec.n_elem,
                                                         (this->Gsb
                                                          % this->Gsb
                                                          % this->bvec))
                           * 2 * M_PI * InvGeV2_to_mb * del_b;
            return sig_el;
        }

        double sig_tot(const UTScheme & S = UTScheme::EIKONAL)
        {
            this->setGsb(S);
            // RUN BASIC TRAPEZOIDAL TO DO THE INTEGRAL TO GET SIG EL AND TOT
            //
            double del_b = this->bvec(1) - this->bvec(0);
            auto sig_tot = o2scl::vector_integ_extended8(this->bvec.n_elem,
                                                         this->Gsb % this->bvec)
                           * 4 * M_PI * InvGeV2_to_mb * del_b;
            return sig_tot;
        }

    protected:
        void setGsb(const UTScheme & S)
        {
            // A1tr  $ \equiv \chi(s,b) $
            vec A1tr = this->hankeltr(this->bvec, this->errvec);

            // Assuming purely imag $ \chi(s,b) $, one obtains a purely imag Gsb:
            //
            vec gsb(A1tr.n_elem);

            switch(S)
            {
                // Eikonal form
                case UTScheme::EIKONAL:
                    gsb = 1.0 - arma::exp(-A1tr);
                    break;

                // U-matrix form
                case UTScheme::UMATRIX:
                    gsb = A1tr / (1.0 + 0.5 * A1tr);
                    break;

                // Error on anything else
                default:
                    throw std::runtime_error("Unhandled Unitarisation scheme.");
            }
            this->Gsb = gsb;
        }

    private:
        double S;
        std::function<double(double)> AmpFunc;
        const arma::vec bvec;
        arma::vec errvec;
        arma::vec Gsb;
        static constexpr double cm_to_inv_GeV = (1.0E14/1.973);
};
