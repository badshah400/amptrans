// vim: set cin et ts=4 sw=4 tw=80:

#include <iostream>
#include <iomanip>
#include <complex>
#include <o2scl/interp.h>
#include <o2scl/vector_derint.h>
#include <functional>
#include <nlopt.hpp>
#include <tuple>
#include <boost/format.hpp>
#include "hankeltrans.hpp"
#include "ampfunc.hpp"
#include "sigdata.hpp"
#include "misc.hpp"

using namespace std;
using namespace o2scl;
using boost::format;

std::function amp_func
    = [] (const double & s, const double & q, const std::vector<double> & par)
      {
          double t = -1.0 * q * q; // Mandelstam variable t

          // $ A(s,t) = s^{1 + \epsilon + 0.5 b_{1} t} / s $
          // See, e.g., Donnachie and Landshoff, Phys.Lett. B727 (2013) 500-505
          double ep = par[0], b_1 = par[1], t_0 = pow(10,par[2]);
          double F1 = (4*sqr(m_p) - 2.79 * t)
                      / (4*sqr(m_p) - t)
                      / sqr(1 - t/t_0); 
          double res = pow(s, ep + 0.5*b_1*t) * sqr(F1);

          // Really small numbers slow up integration, trim them
          // off
          if (abs(res) < std::numeric_limits<double>::epsilon())
            return 0.0;

          return res * std::pow(10,par[3]);
      };



double SigEl(const double & rootS, const std::vector<double> & amp_par)
{
    using namespace std::placeholders;
    double res(0), err(0);
    AmpK A1(rootS, 1E-3);
    A1.setTolAbs(1E-12);
    A1.setTolRel(1E-06);
    std::function <double(const double&, const double &)> f = std::bind(amp_func, _1, _2, amp_par);
    A1.setFunc(f);

    return A1.sig_el(UTScheme::EIKONAL);
}

double SigTot(const double & rootS, const std::vector<double> & amp_par)
{
    using namespace std::placeholders;
    double res(0), err(0);
    AmpK A1(rootS, 1E-3);
    A1.setTolAbs(1E-12);
    A1.setTolRel(1E-06);
    std::function <double(const double&, const double &)> f = std::bind(amp_func, _1, _2, amp_par);
    A1.setFunc(f);

    return A1.sig_tot(UTScheme::EIKONAL);
}


size_t niters = 0;
double chi2(const std::vector<double> & par,
            std::vector<double> & grad,
            void * f_data = nullptr)
{
    niters++;
    sigdata data = *(sigdata *)(f_data);
    cout << format("%5d") % niters;
    for (double p : par)
        cout << format("%15.5f") % p;
        
    vec tot_sqrt_s = data.tot().col(0),
        tot_sig_mb = data.tot().col(1),
        tot_delta  = data.tot().col(2),

        el_sqrt_s  = data.el().col(0),
        el_sig_mb  = data.el().col(1),
        el_delta   = data.el().col(2);

    vec sig_el = el_sqrt_s, sig_tot = tot_sqrt_s;

    sig_el.transform([&] (double val) { return SigEl(val, par); });
    vec sig_el_diff = (sig_el - el_sig_mb) / el_delta;
    double el_res = arma::sum( sig_el_diff % sig_el_diff );

    sig_tot.transform([&] (double val) { return SigTot(val, par); });
    vec sig_tot_diff = (sig_tot - tot_sig_mb) / tot_delta;
    double tot_res = arma::sum( sig_tot_diff % sig_tot_diff );

    double res = tot_res + el_res;
    cout << format("%15.5f") % res << endl;
    return res;
}

int main(int argc, const char * argv[])
{
    using namespace std::complex_literals;
    arma::mat pbarp_el, pp_el;
    sigdata S(100);
    S.set_el ({"../data/rpp2018-pbarp_elastic.dat", "../data/rpp2018-pp_elastic.dat"});
    S.set_tot({"../data/rpp2018-pbarp_total.dat",   "../data/rpp2018-pp_total.dat"});

    // for (size_t i = 0; i < S.num_el(); ++i)
    // {
    //     cout << format("%.5E %15.5f %15.5f") % S.el().col(0)(i)
    //                                          % S.el().col(1)(i)
    //                                          % (S.el().col(2)(i) * 0.5)
    //          << endl;
    // }
    // cout << endl;
    // for (size_t i = 0; i < S.num_tot(); ++i)
    // {
    //     cout << format("%.5E %15.5f %15.5f") % S.tot().col(0)(i)
    //                                          % S.tot().col(1)(i)
    //                                          % (S.tot().col(2)(i) * 0.5)
    //          << endl;
    // }
    // return 0;

    //data_sqrt_s.print();
    //data_sig_mb.print();
    //delta.print();
    //return 0;

    // data_sqrt_s.print();
    
    vector<double> nograd;
    nlopt::opt fmin(nlopt::GN_CRS2_LM, 4);
    fmin.set_min_objective(chi2, &S);
    fmin.set_xtol_abs(1E-6);
    fmin.set_xtol_rel(1E-3);
    fmin.set_maxeval(1E4);

    vector<double> par_bf = {0.1,0.55,0.7,0};
    double chi2_bf;
    //cout << chi2({0.1,0.55,0.7,100}, nograd, &fdata) << endl;
    fmin.set_lower_bounds({1E-2, 0.1,  -2, 0});
    fmin.set_upper_bounds({2E-1, 2.0,   2, 5});
    fmin.optimize(par_bf, chi2_bf);
    cout << format("%|13t|");
    for (double p: par_bf)
        cout << format("%.5f%|15t|") % p;
    cout << format("%.5f") % chi2_bf << endl;

    // Use best-fit to compute sig_el at different energies
    //
    vec sqrt_s_bf = arma::logspace(2, 5, 31);
    vec sig_el_bf(sqrt_s_bf), sig_tot_bf(sqrt_s_bf);
    sig_el_bf.transform([par_bf] (double v) { return SigEl(v, par_bf); } );
    sig_tot_bf.transform([par_bf] (double v) { return SigTot(v, par_bf); } );

    for (size_t i = 0; i < sqrt_s_bf.n_elem; ++i)
        cout << format("%.3E %15.5f %15.5f") % sqrt_s_bf(i)
                                             % sig_el_bf(i)
                                             % sig_tot_bf(i)
             << endl;

    return 0;
}
