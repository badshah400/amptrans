// vim: set cin et ts=4 sw=4 tw=80:

#pragma once

#include <tuple>
#include <armadillo>
#include <utility>
#include <tuple>

inline double sqr(const double & x) { return x * x; }

typedef std::pair<arma::vec,arma::vec> vecvec;

typedef std::tuple<arma::vec,arma::vec,arma::vec> vec3;

// Conversion from GeV^{-2} to milli-barns
static const double InvGeV2_to_mb = sqr(1.9733) * 0.1;

#ifndef vecd
typedef std::vector<double> vecd;
#endif

// Mass of proton
const double m_p = 0.938270;
