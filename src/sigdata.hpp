// vim: set cin et ts=4 sw=4 tw=80:

#pragma once
#include <tabdatrw.hpp>
#include <armadillo>
#include <utility>
#include <string>
#include "misc.hpp"

using arma::mat;
using arma::vec;
typedef std::pair<mat,mat> mat2;

enum SigData {SQRTS = 0, SIG, SIG_DEL};

double pLAB2s(const double & pL, const double & m1 = m_p, const double & m2 = m_p);

class sigdata
{
	public:
        sigdata(const double & thr = 100) :
            sqrt_s_min(thr) { }

        void set_el (const std::vector<std::string> & filenames);
        void set_tot(const std::vector<std::string> & filenames);

        mat el()  { return this->data_el;  }
        mat tot() { return this->data_tot; }

        size_t num_el()  { return this->data_el.n_rows; }
        size_t num_tot() { return this->data_tot.n_rows; }

    private:
        const double sqrt_s_min;
        mat data_el;
        mat data_tot;
};
